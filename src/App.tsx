import React, { useState, useEffect } from 'react';
import './App.css';

import { MyComponent } from '@aiq-ds/react';

function App() {
  const [address, setAddress] = useState({ city: 'Minsk', country: 'BY' });

  const change = () => {
    setAddress({ city: 'London', country: 'UK' });
  }

  return (
    <div className="App">
      <header className="App-header">
        <MyComponent first="John" last="Doe" address={address}></MyComponent>
        <button onClick={() => change()}>Change Address</button>
      </header>
    </div>
  );
}

export default App;
